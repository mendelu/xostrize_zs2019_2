#include <iostream>

using namespace std;

class Clovek{
public:
    string m_jmeno;
    float m_vaha;
    float m_vyska;
    int m_vek;

    void setJmeno(string jmeno){
        m_jmeno = jmeno;
    }

    void setVyska(float vyska){
        if(vyska<0){
            cout<<"Nikdo nemuze byt mensi nez trpaslik" << endl;
        }else{
            m_vyska=vyska;
        }
    }

    void setVaha(float vaha){
        if(vaha>0){
            m_vaha=vaha;
        }else{
            cout<< "Nikdo neni lehci nez pirko!" << endl;
        }
    }

    void setVek(int vek){
        if(vek>0){
            m_vek=vek;
        }else{
            cout<<"Nikdo neni mladsi nez datum narozeni" << endl;
        }
    }
    string getJmeno(){
        return m_jmeno;
    }
    float getVyska(){
        return m_vyska;
    }

    float getVaha(){
        return m_vaha;
    }

    int getVek(){
        return m_vek;
    }

    void printInfo(){
        cout<< "Clovek ----------" << endl;
        cout<< "Jmeno je: " << m_jmeno << endl;
        cout<< "Jmeno je: " << getJmeno() << endl;
        cout<< "Vaha je: \t" << getVaha() << endl;
        cout<< "vyska je: \t" << getVyska() << endl;
        cout<< "vek je: \t"<< getVek() << endl;
        cout<< "BMI je: \t" << getBMI() << endl;
    }

    float getBMI(){
        return (getVyska()*getVyska())/getVaha();
    }
};

int main() {
    Clovek* frantisek = new Clovek();

    frantisek->setJmeno("Frantisek");
    frantisek->setVaha(120);
    frantisek->setVek(30);
    frantisek->setVyska(2.8);
    frantisek->printInfo();
    cout<< "vaha frantiska je:" << frantisek->getVaha() << endl;
    delete frantisek;


    std::cout << "Hello, World!" << std::endl;
    return 0;
}