#include <iostream>

using namespace std;

class Balik{
public:
    string m_prijemce;
    string m_odesilatel;
    float m_vaha;
    float m_vyska;
    float m_sirka;
    float m_hloubka;
    string m_posledniZnamaLokace;
    string m_prijato;

    Balik(string prijemce, float vaha, float vyska, float hloubka, float sirka){
        m_odesilatel = "";
        m_prijemce = prijemce;
        m_vaha= vaha;
        setVyska(vyska);
        m_hloubka = hloubka;
        m_sirka = sirka;
        m_prijato = "dnes";
        m_posledniZnamaLokace= "tady";
    }


    Balik(string prijemce, string odesilatel, float vaha, float vyska, float hloubka, float sirka){
        m_odesilatel = odesilatel;
        m_prijemce = prijemce;
        m_vaha= vaha;
        setVyska(vyska);
        m_hloubka = hloubka;
        m_sirka = sirka;
        m_prijato = "dnes";
        m_posledniZnamaLokace= "tady";
    }

    void setVyska(float vyska){
        if(vyska>0){
            m_vyska = vyska;
        }else{
            cout<< "balik je priliz lehky prirazuji 0"<< endl;
            m_vyska=0;
        }
    }

    float vypocitatObjem(float sirka, float vyska, float hloubka){
        return sirka*vyska*hloubka;
    }

    float vypocitatObjem(){
        return m_vyska*m_sirka*m_hloubka;
    }

    void printInfo(){
        cout<< "Balik ------------------" << endl;
        cout<< "Prijemce: \t" << m_prijemce << endl;
        cout<< "Odesilatel: \t" << m_odesilatel << endl;
        cout<< "Rozmery [s v h]:" << m_sirka << " " << m_vyska << " " << m_hloubka << endl;
        cout<< "Objem Baliku: \t" << vypocitatObjem() << endl;
        cout<< "Posledni loakce: \t" << m_posledniZnamaLokace << endl;
        cout<< "Prijato: \t" << m_prijato << endl;

    }

    ~Balik(){
        cout<< "balik byl dorucen \njiz ho neni treba evidovat" << endl;
    }

};


int main() {
    Balik* postovniBalik= new Balik("Frantisek Ostrizek", 0.5, -1.2, 3, 5);

    postovniBalik->printInfo();


    cout<< "objem baliku:" << postovniBalik->vypocitatObjem(0.8,0.9,1) << endl;
    delete(postovniBalik);

    Balik* pplBalik = new Balik("Frantisek Ostrizek", "Ondrej Svehla", 0.4, 5, 8, 1);
    pplBalik->printInfo();
    delete(pplBalik);

    return 0;
}