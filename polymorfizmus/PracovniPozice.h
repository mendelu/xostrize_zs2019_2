//
// Created by xostrize on 22.11.2019.
//

#ifndef POLYMORFIZMUS_PRACOVNIPOZICE_H
#define POLYMORFIZMUS_PRACOVNIPOZICE_H

class PracovniPozice{
public:
    virtual int getDniDovolene(int cerpani) = 0;
    virtual int getPlat(int letVeFirme, int studentVzdelani) = 0;
};

#endif //POLYMORFIZMUS_PRACOVNIPOZICE_H
