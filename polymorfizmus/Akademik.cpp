//
// Created by xostrize on 22.11.2019.
//

#include "Akademik.h"

int Akademik::getDniDovolene(int cerpani) {
    const int maxDniDovolena =50;
    return maxDniDovolena-cerpani;
}

int Akademik::getPlat(int letVeFirme, int studentVzdelani) {
    const int mzakladniPlat = 40000;
    const int rocniBonus = 5000;

    return mzakladniPlat+studentVzdelani*rocniBonus;
}
