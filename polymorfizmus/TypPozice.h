//
// Created by xostrize on 22.11.2019.
//

#ifndef POLYMORFIZMUS_TYPPOZICE_H
#define POLYMORFIZMUS_TYPPOZICE_H

enum class TypPozice{
    technik, akademik
};

#endif //POLYMORFIZMUS_TYPPOZICE_H
