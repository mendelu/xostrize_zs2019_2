//
// Created by xostrize on 22.11.2019.
//

#ifndef POLYMORFIZMUS_TECHNIK_H
#define POLYMORFIZMUS_TECHNIK_H

#include "PracovniPozice.h"

class Technik : public PracovniPozice {

public:
    int getDniDovolene(int cerpani) override;

    int getPlat(int letVeFirme, int studentVzdelani) override;

};


#endif //POLYMORFIZMUS_TECHNIK_H
