#include <iostream>
#include "Zamestnanec.h"
#include "TypPozice.h"

using namespace std;

int main(){
    Zamestnanec* pepa= new Zamestnanec(3,1,TypPozice::technik);

    cout<< "vyplata:\t" << pepa->getPlat() << endl;
    cout<< "dni dovolene:\t"<< pepa->getZbyvaDovolene() << endl;

    cout<< "cerpaniDovolene" << endl;
    pepa->evidujDovolenou(5);

    cout<< "vyplata:\t" << pepa->getPlat() << endl;
    cout<< "dni dovolene:\t"<< pepa->getZbyvaDovolene() << endl;

    cout<< "zmena pracovniho zarazeni" << endl;
    pepa->zmenPracovniPozici(TypPozice::akademik);

    cout<< "vyplata:\t" << pepa->getPlat() << endl;
    cout<< "dni dovolene:\t"<< pepa->getZbyvaDovolene() << endl;
    delete(pepa);
    return 0;
}