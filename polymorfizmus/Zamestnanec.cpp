//
// Created by xostrize on 22.11.2019.
//

#include "Zamestnanec.h"
Zamestnanec::Zamestnanec(int letVeFirme, int stupenVzdelani,TypPozice pracovniPozice){
    m_pozice = nullptr;
    zmenPracovniPozici(pracovniPozice);

    m_letVeFirme=letVeFirme;
    m_stupenVzdelani=stupenVzdelani;
    m_cerpanoDniDovolene=0;
}
Zamestnanec::~Zamestnanec(){
    delete(m_pozice);
}

int Zamestnanec::getPlat(){
    return m_pozice->getPlat(m_letVeFirme,m_stupenVzdelani);
}
int Zamestnanec::getZbyvaDovolene(){
    return m_pozice->getDniDovolene(m_cerpanoDniDovolene);
}
void Zamestnanec::evidujDovolenou(int cerpaniDni){
    m_cerpanoDniDovolene+= cerpaniDni;
}

void Zamestnanec::zmenPracovniPozici(TypPozice pracovniPozice){
    if(m_pozice != nullptr){
        delete(m_pozice);
    }

    switch (pracovniPozice){
        case TypPozice::akademik :
            m_pozice = new Akademik();
            break;
        case TypPozice::technik :
            m_pozice = new Technik();
            break;
        default:
            std::cout<< "spatna pracovni pozice" << std::endl;
            m_pozice= nullptr;
    }
}