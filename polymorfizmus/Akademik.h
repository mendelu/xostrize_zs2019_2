//
// Created by xostrize on 22.11.2019.
//

#ifndef POLYMORFIZMUS_AKADEMIK_H
#define POLYMORFIZMUS_AKADEMIK_H

#include "PracovniPozice.h"

class Akademik :public PracovniPozice {

public:
    int getDniDovolene(int cerpani) override;

    int getPlat(int letVeFirme, int studentVzdelani) override;
};


#endif //POLYMORFIZMUS_AKADEMIK_H
