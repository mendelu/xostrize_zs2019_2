//
// Created by xostrize on 25.10.2019.
//

#ifndef VRAKOVISTE_VRAK_H
#define VRAKOVISTE_VRAK_H

#include "Vrak.h"
#include <iostream>

using namespace std;

class Vrak {
private:
    string m_popis;
    float m_weight;
    int m_cena;

public:
    Vrak(string content, float weight, int cena);

    /*
     * ale může se stát, že vrak je neznámý.
 * V takovém případě se pouze zadá váha a cena je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
     * */
    Vrak(float weight);

    int getCena();

    float getWeight();

    string getContent();

};


#endif //VRAKOVISTE_VRAK_H
