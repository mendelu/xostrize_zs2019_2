//
// Created by xostrize on 25.10.2019.
//

#ifndef VRAKOVISTE_VRAKOVISTE_H
#define VRAKOVISTE_VRAKOVISTE_H

#include "Vrak.h"
#include <iostream>

using namespace std;

class Vrakoviste {
private:
    float m_totalWeight;
    int m_totalPieces;
public:
    Vrakoviste();
//Třída obsahuje metodu uložVrak, která převezme ukazatel na vrak a přidá do celkové váhy a počtu kusů hodnoty z vraku. (1 bod)
    void storeVrak(Vrak* storedVrak);


    /*Dále třída obsahuje metodu odeberVrak, která opět převezme ukazatel na kontejner a odečte z celkových hodnot hodnoty vraku.
    * Proběhne však kontrola a pokud je ve vrakovišti menší váha nebo menší počet kusů vraků,
    * než je uvedeno ve vrakovišti, odečet se neprovede. (2 body)*/
    void removeVrak(Vrak* storedVrak);
// * Poslední metodou je metoda printInfo vypisující obsah vrakoviště na obrazovku (1 bod).
    void printInfo();
};


#endif //VRAKOVISTE_VRAKOVISTE_H
