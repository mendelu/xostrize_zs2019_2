/**
 * Vytvořte informační systém vrakoviště, který eviduje vraky a nahradní díly.
 * Každý vrak obsahuje textový popis vraku, váhu (des. číslo) a cenu (celé číslo).
 * Všechny tyto informace jsou povinné (vynucené při vytvoření nahradního dílu), ale může se stát, že vrak je neznámý.
 * V takovém případě se pouze zadá váha a cena je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
 *
 * Dále program obsahuje třídu Vrakoviště, která představuje místo do kterého se vraky.
 * Vrakoviště má vlastnosti celkovvou váhu a celkový počet kusů. Na počátku jsou oba nastaveny na 0. (1 bod konst.)
 * Třída obsahuje metodu uložVrak, která převezme ukazatel na vrak a přidá do celkové váhy a počtu kusů hodnoty z vraku. (1 bod)
 * Dále třída obsahuje metodu odeberVrak, která opět převezme ukazatel na kontejner a odečte z celkových hodnot hodnoty vraku.
 * Proběhne však kontrola a pokud je ve vrakovišti menší váha nebo menší počet kusů vraků,
 * než je uvedeno ve vrakovišti, odečet se neprovede. (2 body)
 * Poslední metodou je metoda printInfo vypisující obsah vrakoviště na obrazovku (1 bod).
 *
 * V hlavní funkci programu vytvořte vrakoviště a dva vraky, uložte je do kontejneru a vypište jeho stav. (2 body)
 *
 */


#include <iostream>
#include "Vrak.h"
#include "Vrakoviste.h"

using namespace std;


//* V hlavní funkci programu vytvořte vrakoviště a dva vraky, uložte je do kontejneru a vypište jeho stav. (2 body)
int main(int argc, const char * argv[]) {
    Vrakoviste* v1 = new Vrakoviste();
    Vrak* vrak1 = new Vrak("skoda", 7.2, 1);
    Vrak* vrak2 = new Vrak(1);
    v1->storeVrak(vrak1);
    v1->storeVrak(vrak2);
    v1->printInfo();
    delete vrak1;
    delete vrak2;
    delete v1;
    return 0;
}