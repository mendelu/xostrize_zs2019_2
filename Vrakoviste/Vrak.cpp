//
// Created by xostrize on 25.10.2019.
//

#include "Vrak.h"


Vrak::Vrak(string content, float weight, int cena){
m_popis = content;
m_cena = cena;
m_weight = weight;
}

/*
 * ale může se stát, že vrak je neznámý.
* V takovém případě se pouze zadá váha a cena je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
 * */
Vrak::Vrak(float weight){
    m_popis = "unknown";
    m_cena = 0;
    m_weight = weight;
}

int Vrak::getCena(){
    return m_cena;
}

float Vrak::getWeight(){
    return m_weight;
}

string Vrak::getContent(){
    return m_popis;
}
