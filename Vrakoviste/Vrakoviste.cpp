//
// Created by xostrize on 25.10.2019.
//

#include "Vrakoviste.h"


Vrakoviste::Vrakoviste(){
    m_totalPieces = 0;
    m_totalWeight = 0;
}
//Třída obsahuje metodu uložVrak, která převezme ukazatel na vrak a přidá do celkové váhy a počtu kusů hodnoty z vraku. (1 bod)
void Vrakoviste::storeVrak(Vrak* storedVrak){
    m_totalPieces ++;
    m_totalWeight += storedVrak->getWeight();
}


/*Dále třída obsahuje metodu odeberVrak, která opět převezme ukazatel na kontejner a odečte z celkových hodnot hodnoty vraku.
* Proběhne však kontrola a pokud je ve vrakovišti menší váha nebo menší počet kusů vraků,
* než je uvedeno ve vrakovišti, odečet se neprovede. (2 body)*/
void Vrakoviste::removeVrak(Vrak* storedVrak){
    if (storedVrak->getCena() < m_totalPieces){
        m_totalPieces --;
    }
    if (storedVrak->getWeight() < m_totalWeight){
        m_totalWeight -= storedVrak->getWeight();
    }
}
// * Poslední metodou je metoda printInfo vypisující obsah vrakoviště na obrazovku (1 bod).
void Vrakoviste::printInfo(){
    cout << "Weight: " << m_totalWeight << endl;
    cout << "Items: " << m_totalPieces << endl;
}