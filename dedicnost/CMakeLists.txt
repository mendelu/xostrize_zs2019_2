cmake_minimum_required(VERSION 3.14)
project(dedicnost)

set(CMAKE_CXX_STANDARD 14)

add_executable(dedicnost main.cpp Osoba.cpp Osoba.h Ucitel.cpp Ucitel.h Student.cpp Student.h Uis.cpp Uis.h)