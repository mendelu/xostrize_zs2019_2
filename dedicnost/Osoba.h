//
// Created by xostrize on 15.11.2019.
//

#ifndef DEDICNOST_OSOBA_H
#define DEDICNOST_OSOBA_H

#include <iostream>

using namespace std;

/*
 * Každý student bude ponese jmeno, rodné číslo, semestr ve kterém studuje a studijní průměr. Všechny potřebné údaje budou vynuceny konstruktorem a budou modifikovány příslušnými getry a setry. Mějme metodu printInfo, která nám bude sloužit jako kontrolní výpis.

Každý učitel vlastní jméno, rodné číslo a ústav na kterém pracuje. Všechny potřebné údaje budou vynuceny konstruktorem a budou modifikovány příslušnými getry a setry. Mějme metodu printInfo, která nám bude sloužit jako kontrolní výpis.
 * */

class Osoba {
private:
    // společné parametry studenta a učitele
    string m_jmeno;
    string m_rodneCislo;
public:
    // konstruktor dle zadaní
    Osoba(string jmeno, string rodneCislo);

    void printInfo();
    string getJmeno();
    string getRodneCislo();
    void setJmeno(string jmeno);


};


#endif //DEDICNOST_OSOBA_H
