#include <iostream>
#include "Osoba.h"
#include "Ucitel.h"
#include "Student.h"
#include "Uis.h"


int main() {
   Osoba* osoba= new Osoba("Frantisek", "123456789");
   osoba->printInfo();
   delete(osoba);

   Ucitel* ucitel=new Ucitel("Tomas", "987654321","Department of Informatics");
   ucitel->printInfo();
   delete(ucitel);


   Osoba* osoba1 = new Ucitel("Jan", "654789321","Department of statistics");
   osoba1->printInfo();
   delete osoba1;

    Student* student = new Student("Petr", "14788963", 5, 1.5);
    student->printInfo();
    delete(student);

    cout<<"Uis ######################" <<endl;

    Uis* uis = new Uis();
    uis->addStudent("Jakub", "654987321", 1, 1.5);
    uis->addUcitel("Tomas", "1478963", "Department of statistics");
    uis->printInfo();
    delete(uis);

    return 0;
}