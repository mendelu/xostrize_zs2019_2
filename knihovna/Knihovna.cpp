//
// Created by xostrize on 08.11.2019.
//

#include "Knihovna.h"

int Knihovna::s_id=0;

Knihovna::Knihovna() {
    s_id++;
    m_id_knihovna=s_id;
}


Knihovna* Knihovna::createKnihovna(){
    return new Knihovna();
}
void Knihovna::printInfo(){
    cout<< "Knihovna "<< m_id_knihovna << "///////////////" << endl;
    for(Kniha* kn: m_knihy){
        kn->printInfo();
    }

}
void Knihovna::addBook(Kniha* kniha){
    m_knihy.push_back(kniha);
}
void Knihovna::createBook(string nazev,string autor){
    m_knihy.push_back(new Kniha(nazev, autor));
}
void Knihovna::searchForAuthor(string autor){
    for(Kniha* kn: m_knihy){
        if(kn->getAutor()==autor){
            kn->printInfo();
        }
    }

}
void Knihovna::removeBook(int id){
    for(int i=0; i<m_knihy.size(); i++){
        if(m_knihy.at(i)->getId()==id){
            delete(m_knihy.at(i));
            m_knihy.erase(m_knihy.begin()+i);
        }
    }
}