//
// Created by xostrize on 08.11.2019.
//

#include "Kniha.h"


int Kniha::s_id=0;

Kniha::Kniha(string nazev, string autor){
    m_nazev=nazev;
    m_autor=autor;
    s_id++;
    m_id=s_id;
}
void Kniha::printInfo(){
    cout<< "Kniha ----------------" << endl;
    cout<< "autor: \t" <<m_autor << endl;
    cout<< "nazev: \t" << m_nazev << endl;
    cout<< "id: \t" << m_id << endl;
}
string Kniha::getAutor(){
    return m_autor;
}
int Kniha::getId(){
    return m_id;
}