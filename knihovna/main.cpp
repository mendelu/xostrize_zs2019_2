#include <iostream>

#include "Kniha.h"
#include "Knihovna.h"

int main() {
    Knihovna* knihovna = Knihovna::createKnihovna();

    Kniha* kniha= new Kniha("Eledest", "christopher paoliny");

    knihovna->addBook(kniha);
    knihovna->createBook("Eragon", "christopher paoliny");

    knihovna->printInfo();

    cout<< "nalezeni knih podle autora" << endl;

    knihovna->searchForAuthor("christopher paoliny");

    cout<< "zkouska mazani knihy" << endl;

    knihovna->removeBook(1);

    knihovna->printInfo();

    delete(kniha);
    delete(knihovna);
    return 0;
}