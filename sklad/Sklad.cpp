//
// Created by xostrize on 01.11.2019.
//

#include "Sklad.h"


Sklad::Sklad(string adresa){
    m_adresa=adresa;
    m_patra.push_back(new Patro("Prizemi"));
    m_idexer=0;
}
Sklad::~Sklad(){
    for(Patro* patr:m_patra){
        delete(patr);
    }
}
void Sklad::postavPatro(){
    m_idexer++;
    m_patra.push_back(new Patro(to_string(m_idexer)));
}
void Sklad::bourejPatro(){
    m_idexer--;
    delete(m_patra.at(m_patra.size()-1));
    m_patra.pop_back();
}
void Sklad::ulozKontejner(int patro, int pozice, Kontejner* kont){}
Kontejner* Sklad::odeberKontejner(int patro, int pozice){
    return nullptr;
}
void Sklad::vypisObsah(){
    cout<< "sklad: \t" << m_adresa << "#####################" << endl;
    for(Patro* patr:m_patra){
        patr->vypisObsah();
    }
}