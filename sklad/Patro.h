//
// Created by xostrize on 01.11.2019.
//

#ifndef SKLAD_PATRO_H
#define SKLAD_PATRO_H

#include <iostream>
#include <array>
#include "Kontejner.h"

using namespace std;

class Patro {
private:
    string m_oznaceni;
    array<Kontejner*, 10> m_pozice;
public:
    Patro(string oznaceni);
    void ulozKontejner(int pozice, Kontejner* kont);
    Kontejner* odeberKontejner(int ktery);
    void vypisObsah();
};


#endif //SKLAD_PATRO_H
