//
// Created by xostrize on 01.11.2019.
//

#include "Patro.h"


Patro::Patro(string oznaceni){
    m_oznaceni=oznaceni;

    for(Kontejner* &pozice:m_pozice){
        pozice= nullptr;
    }

}
void Patro::ulozKontejner(int pozice, Kontejner* kont){
    if(pozice >= 0 and pozice <=10){
        m_pozice.at(pozice)=kont;
    }else{
        cout<< "pristupujes na misto ke kteremu nemas pravo" << endl;
    }
}
Kontejner* Patro::odeberKontejner(int ktery){
    if(ktery >= 0 and ktery <=10){
        Kontejner* kont=m_pozice.at(ktery);
        m_pozice.at(ktery)= nullptr;
        return kont;
    }else{
        cout<< "pristupujes na misto ke kteremu nemas pravo" << endl;
        return nullptr;
    }
}
void Patro::vypisObsah(){
    cout<< "patro: \t" << m_oznaceni << "-----------------" << endl;

    for(int i= 0; i< m_pozice.size();i++){
        cout<< "pozice "<< i << ":";
        if(m_pozice.at(i)== nullptr){
            cout<<" prazdna pozice"<< endl;
        }else{
            cout<< endl;
            cout<< "Majitel: \t" << m_pozice.at(i)->getNazev() << endl;
            cout<< "Vaha: \t" << m_pozice.at(i)->getVaha() << endl;
            cout<< "Obsah: \t" << m_pozice.at(i)->getObjem() << endl;
        }

    }

}