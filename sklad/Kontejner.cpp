//
// Created by xostrize on 01.11.2019.
//

#include "Kontejner.h"


Kontejner::Kontejner(string nazev, float vaha, float objem){
    m_nazev=nazev;
    m_objem=objem;
    m_vaha=vaha;
}
float Kontejner::getVaha(){
    return m_vaha;
}
float Kontejner::getObjem(){
    return m_objem;
}
string Kontejner::getNazev(){
    return m_nazev;
}