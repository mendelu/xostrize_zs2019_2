//
// Created by xostrize on 01.11.2019.
//

#ifndef SKLAD_SKLAD_H
#define SKLAD_SKLAD_H

#include <iostream>
#include <vector>
#include "Patro.h"
#include "Kontejner.h"

using namespace std;

class Sklad {
private:
    string m_adresa;
    vector<Patro*> m_patra;
    int m_idexer;

public:
    Sklad(string adresa);
    ~Sklad();
    void postavPatro();
    void bourejPatro();
    void ulozKontejner(int patro, int pozice, Kontejner* kont);
    Kontejner* odeberKontejner(int patro, int pozice);
    void vypisObsah();

};


#endif //SKLAD_SKLAD_H
