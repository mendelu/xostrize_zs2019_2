//
// Created by xostrize on 01.11.2019.
//

#ifndef SKLAD_KONTEJNER_H
#define SKLAD_KONTEJNER_H

#include <iostream>

using namespace std;

class Kontejner {
private:
    string m_nazev;
    float m_vaha;
    float m_objem;
public:
    Kontejner(string nazev, float vaha, float objem);

    float getVaha();
    float getObjem();
    string getNazev();


};


#endif //SKLAD_KONTEJNER_H
